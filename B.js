(function($){
    $.widget('chessboard.B', $.chessboard.piece, {

        _initComponent: function() {
            var me = this;
            me.spriteCoordinates = {x: 480, y: me.spriteLine};
        },

        _calculateMoves: function() {
            var me = this,
            explorer = me.getBoard()._explorer(me),
            moves = [
                explorer.diagonal('front', 'left'),
                explorer.diagonal('front', 'right'),
                explorer.diagonal('back', 'left'),
                explorer.diagonal('back', 'right')
            ];

            return moves;
        }

    });
})(jQuery);