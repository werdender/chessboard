(function($){
    $.widget('chessboard.chessboard', {

        options: {
            size: 480,
            flip: false,
            resizable: true,
            validateMoves: true,
            enableSound: true,
            board: false,
            maxSize: 960,
            minSize: 120,
            cellSize: null,
            figureHeight: null,
            cellsCoordinates: [],
            blackColor: '#b58863',
            whiteColor: '#f0d9b5',
            imagesDir: 'chessboard/images',
            soundsDir: 'chessboard/sounds',
            figuresSprite: 'f.png',
            FEN: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
        },

        whiteMovesCount: 0,
        blackMovesCount: 0,
        moveColor: 'w',

        _explorer: function(slave) {
            var me = this,
            explorer = {
                _stepV: function(n, direction) {
                    var row = slave.getCell().row,
                    res = {
                        c: slave.getCell().column,
                        r: null
                    };

                    if(slave.getColor('w')) {
                        res.r = direction == 'front' ? row + n : row - n;
                    }else{
                        res.r = direction == 'front' ? row - n : row + n;
                    }
                    return res;
                },


                _stepH: function(n, direction){
                    var column = slave.getCell().column,
                    res = {
                        c: null,
                        r: slave.getCell().row
                    };

                    if(slave.getColor('w')) {
                        if(me.isFlipped()) {
                            res.c = direction == 'right' ? column - n : column + n;
                        }else{
                            res.c = direction == 'right' ? column + n : column - n;
                        }
                    }else{
                        if(me.isFlipped()) {
                            res.c = direction == 'right' ? column + n : column - n;
                        }else{
                            res.c = direction == 'right' ? column - n : column + n;
                        }
                    }
                    return res;
                },

                _stepD: function(n, direction) {
                    var res = {
                        c: explorer._stepH(n, direction[0]).c,
                        r: explorer._stepV(n, direction[1]).r
                    };
                    return res;
                },

                _getMap: function(stepFn, direction, count) {
                    var map = [],
                    currentCell,
                    step;

                    count = count ? (count + 1) : 9;
                    for(var i = 1; i < count; i++) {
                        step = stepFn(i, direction);
                        currentCell = me._getCellByCR(step.r, step.c);
                        if(currentCell) {
                            map.push({
                                cell: currentCell,
                                brokenCellAlias: currentCell.alias
                            });
                        }
                    }
                    return map;
                },

                custom: function(c, bc) {
                    var vertical = explorer._stepV(c.vertical.count, c.vertical.direction),
                    horizontal = explorer._stepH(c.horizontal.count, c.horizontal.direction),
                    brokenV,
                    brokenH,
                    brokenCell,
                    cell = me._getCellByCR(vertical.r, horizontal.c);

                    if( ! cell) {
                        return [];
                    }

                    if(bc) {
                        brokenV = explorer._stepV(bc.vertical.count, bc.vertical.direction),
                        brokenH = explorer._stepH(bc.horizontal.count, bc.horizontal.direction),
                        brokenCell = me._getCellByCR(brokenV.r, brokenH.c);
                    }else if(bc !== false) {
                        brokenCell = cell;
                    }else{
                        brokenCell = false;
                    }
                    return [{
                        cell: me._getCellByCR(vertical.r, horizontal.c),
                        brokenCellAlias: brokenCell ? brokenCell.alias : false
                    }];
                },

                diagonal: function(vDirection, hDirection, count) {
                    return explorer._getMap(explorer._stepD, [hDirection, vDirection], count);
                },

                vertical: function(direction, count) {
                    return explorer._getMap(explorer._stepV, direction, count);
                },

                horizontal: function(direction, count) {
                    return explorer._getMap(explorer._stepH, direction, count);

                },

                lastEmpty: function(moves) {
                    var res = [];

                    $.each(moves, function(y, step) {
                        if(step.cell.figure) {
                            return false;
                        }else{
                            res.push(step);
                        }
                    });

                    return res;
                },

                lastEmptyOrEdible: function(moves) {
                    var res = [];

                    $.each(moves, function(y, step) {
                        if(step.cell.figure) {
                            if(step.cell.figure.getColor() == slave.getColor()) {
                                return false;
                            }else{
                                res.push(step);
                                return false;
                            }
                        }else{
                            //if(me.mainFigure.componentId == me.componentI && me._isSafeMove(me, step.cell, true))
                            res.push(step);
                        }
                    });
                    return res;
                }
            };
            return explorer;
        },

        _create: function () {
            var me = this;

            me._getEl().uniqueId();
            me.componentId = me._getEl().attr('id');
            me.boardSize = me.options.size;
            me.cellSize = me._calcCellSize();
            me.figureHeight = me.options.figureHeight > me.cellSize ? me.options.figureHeight : me.cellSize;
            me.nextMoveColor = 'w';
            me.currentMove = 0;
            me.cells = {
                a:[],
                b:[],
                c:[],
                d:[],
                e:[],
                f:[],
                g:[],
                h:[]
            };
            me.cellsCRIndex = {1:[], 2:[], 3:[], 4:[], 5:[], 6:[], 7:[], 8:[]};
            me.cellsCoordinatesIndex = {};
            me.figuresSprite = new Image(),
            me.figuresSprite.src = me._getOpts().imagesDir + '/' + me._getOpts().figuresSprite;
            me.mainFigure = {b: false, w: false};

            me.figuresSprite.onload = function() {
                me._initComponent();
            };


        },

        _initComponent: function() {
            var me = this;

            me._render();
            if(me.options.resizable) {
                me._getEl().on('resizestart', $.proxy(me._onBeforeResize, me));
                me._getEl().on('resizestop', $.proxy(me._onAfterResize, me));
                $(me.canvas).resizable({
                    aspectRatio: 1/1,
                    maxHeight: me.options.maxSize,
                    maxWidth: me.options.maxSize,
                    minHeight: me.options.minSize,
                    minWidth: me.options.minSize,
                    grid: 8,
                    resize: $.proxy(me._onResize, me)
                });
            }

            if(me.options.enableSound) {
                me._addSounds();
            }

            me._getEl().on('figuremove', $.proxy(me._onFigureMove, me));
            me._getEl().on('figureX', $.proxy(me._onFigureX, me));
            me._getEl().on('figuresready', $.proxy(me._onFiguresReady, me));
            me._getEl().on('reshuffle', $.proxy(me._onReshuffle, me));
            me._getEl().on('check', $.proxy(me._onCheck, me));
            me._getEl().on('gameover', $.proxy(me._onGameOver, me));
            me._getEl().on('reincarnation', $.proxy(me._onFigureReincarnation, me));
            //me.pos('rnbqkb1r/ppp2ppp/3p3n/8/2B1Pp2/2N2N2/PPPP2PP/R1BQ1K1R w kq -');
            //me.posByPGN();

            me.pgn = '[Date "????.??.??"]'+
            '[Result "*"]'+
            '[FEN "rnbqkb1r/ppp2ppp/3p3n/8/2B1Pp2/2N2N2/PPPP2PP/R1BQ1K1R w kq -"]'+

            '1.Nd5 c6 2.Nb4 d5 '+
            '*';

            me._initCells();
            me.placeFigures(me.pgn);
        //me._getEl().draggable();
        },

        _destroy: function () {

        },

        audioCollection: {},
        brokenCells: {
            curr: {all: {}, b: {}, w: {}, bindex: [], windex: []},
            agressors: {},
            previousCurr: {},
            previousAgressors: {}
        },

        _brokenCells: function() {
            var me = this,
            brokenCells = {
                clear: function() {
                    me.brokenCells.previousAgressors = me.brokenCells.agressors;
                    me.brokenCells.previousCurr = me.brokenCells.curr;
                    me.brokenCells.agressors = {};
                    me.brokenCells.curr = {all: {}, b: {}, w: {}, bindex: [], windex: []};
                },
                add: function(cellAlias, figure) {
                    if( ! me.brokenCells.curr.all[figure.componentId]) me.brokenCells.curr.all[figure.componentId] = [];
                    if( ! me.brokenCells.curr[figure.getColor()][figure.componentId]) me.brokenCells.curr[figure.getColor()][figure.componentId] = [];
                    me.brokenCells.curr.all[figure.componentId].push({cell: cellAlias, figure: figure});
                    me.brokenCells.curr[figure.getColor()][figure.componentId].push({cell: cellAlias, figure: figure});
                    me.brokenCells.curr[figure.getColor() + 'index'].push(cellAlias);
                    if(cellAlias && ! me.brokenCells.agressors[cellAlias]) {
                        me.brokenCells.agressors[cellAlias] = [];
                    }
                    me.brokenCells.agressors[cellAlias].push(figure);
                },
                get: function(actuality, color, asArrayOfAliases) {
                    if(asArrayOfAliases) {
                        return me.brokenCells[actuality][color + 'index'];
                    }
                    var cells = [];

                    $.each(me.brokenCells[actuality][color], function(i, figure) {
                        $.each(figure, function(i, bc) {
                            cells.push(bc);
                        });
                    });
                    return cells;
                },
                current: function(color, asArrayOfAliases) {
                    if( ! color) color = 'all';

                    return brokenCells.get('curr', color, asArrayOfAliases);
                },
                agressors: function(cellAlias) {
                    //if( ! color) color = 'all';
                    return me.brokenCells.agressors[cellAlias] || [];
                },
                del: function(type, figure) {
                    var brake = false;

                    $.each(me.brokenCells.curr[type], function(i, f) {
                        $.each(f, function(y, bc) {
                            if(bc.figure.componentId == figure.componentId) {
                                delete me.brokenCells.curr[type][figure.componentId];
                                brake = true;
                                return false;
                            }
                        });
                        if(brake) return false;
                    });
                },
                remove: function(figure) {
                    brokenCells.del('all', figure);
                    brokenCells.del(figure.getColor(), figure)
                }
            };
            return brokenCells;
        },

        _audio: function(){
            var me = this;
            me.player = me.player || {
                play: function(name) {
                    if( ! me._getOpts().enableSound) {
                        return false;
                    }
                    me.audioCollection[name].play();
                },

                add: function(name, urls) {
                    var a = document.createElement('audio'),
                    nocache = '_ds='+new Date().getTime(),
                    sources = [document.createElement('source'), document.createElement('source')];
                    a.preload = 'auto';
                    a.id = me.componentId + '-audio-' + name;
                    sources[0].src = urls[0] + '?' + nocache;
                    sources[1].src = urls[1] + '?' + nocache;
                    a.appendChild(sources[0]);
                    a.appendChild(sources[1]);
                    document.body.appendChild(a);
                    me.audioCollection[name] = a;
                }
            }
            return me.player;
        },

        _setOption: function (key, value) {
            this._super( "_setOption", key, value );
        },

        _render: function() {
            var me = this;

            me.canvas = document.createElement('canvas');
            me.canvas.width = me.boardSize;
            me.canvas.height = me.boardSize;
            me.canvas.id = me.componentId + '-board-canvas';

            me.wrapper = $(document.createElement('div'));
            me._getEl().append(me.wrapper);
            me._getWrapper().css({
                position: 'relative',
                width: me.boardSize,
                height: me.boardSize,
                border: 0,
                'z-index': 10
            });

            var ctx = me.canvas.getContext('2d');

            if( ! me.options.board) {
                ctx.fillStyle = me.options.blackColor; // draw cells
                ctx.fillRect(0, 0, me.boardSize, me.boardSize);
                for (var i = 0; i < 8; i += 2) {
                    for (var j = 0; j < 8; j += 2) {
                        ctx.fillStyle = me.options.whiteColor;
                        ctx.fillRect(i * me.cellSize, j * me.cellSize, me.cellSize, me.cellSize);
                        ctx.fillRect((i + 1) * me.cellSize, (j + 1) * me.cellSize, me.cellSize, me.cellSize);
                    }
                }
                ctx.strokeStyle = me.options.blackColor; // draw board frame
                ctx.strokeRect(0, 0, me.boardSize, me.boardSize);
            }else{
                var board = new Image(); // draw user defined board
                board.src = me.options.board;
                board.onload = function() {
                    ctx.drawImage(board, 0, 0, me.boardSize, me.boardSize);
                }
            }
            $(me.canvas).css({
                position: 'absolute',
                width: me.boardSize,
                height: me.boardSize
            });
            me._getWrapper().append(me.canvas);
        },

        _addSounds: function() {
            var me = this,
            dir = me._getOpts().soundsDir;
            me._audio().add('X', [dir + '/piece_x.ogg', dir + '/piece_x.mp3']);
            me._audio().add('move', [dir + '/piece_move.ogg', dir + '/piece_move.mp3']);
            me._audio().add('game_start', [dir + '/game_start.ogg', dir + '/game_start.mp3']);
            me._audio().add('game_over', [dir + '/game_over.ogg', dir + '/game_over.mp3']);
            me._audio().add('check', [dir + '/check.ogg', dir + '/check.mp3']);
            me._audio().add('reshuffle', [dir + '/reshuffle.ogg', dir + '/reshuffle.mp3']);
        },


        _getEl: function() {
            return this.element;
        },

        _getOpts: function() {
            return this.options;
        },

        _onBeforeResize: function() {
            this.getFigures().hide();
        },

        _onResize: function(event, ui) {
            var me = this;
            me.boardSize = ui.size.width;
            me.cellSize = me._calcCellSize();
        //me._calcCellsCoordinates();
        },

        _onAfterResize: function(event, ui) {
            var me = this;

            me.boardSize = ui.size.width;
            me.cellSize = this._calcCellSize();
            me._calcCellsCoordinates();
            me._getWrapper().css({
                width: me.boardSize,
                height: me.boardSize
            });
            me.getFigures().show();
        },

        _onFigureMove: function(event, board, figure, cellFrom, cellTo) {
            var me = this;

            //setImmediate(function() {
            me._audio().play('move');
            //})
            //me._updateCurrentMove(figure.getColor());
        },

        _onFigureX: function(event, figure) {
            this._audio().play('X');
        },

        _onFiguresReady: function(event, board) {
            this._updateFiguresMoves();
            this._audio().play('game_start');
        },

        _onCheck: function(event, king, aggressor) {
            var me = this;
            //setImmediate(function() {
            me._audio().play('check');
            //});
        },

        _onGameOver: function(event, board, color) {
            this._audio().play('game_over');
        },

        _onReshuffle: function(event, type, king, rook, color) {
            this._audio().play('reshuffle');
        },

        _onFigureReincarnation: function(event, figure) {
            //console.log(figure)
        },

        _toggleMoveColor: function() {
            var me = this;
            if(me.moveColor == 'w') {
                me.whiteMovesCount ++;
                me.currentMove++;
                me.moveColor = 'b';
            }else{
                me.blackMovesCount ++;
                if(me.currentMove === 0) {
                    me.currentMove++;
                }
                me.moveColor = 'w';
            }
        },

        _initCells: function() {
            var me = this, r = 1;

            $.each(me.cells, function(cell) {
                //var c = me.isFlipped ? 1 : 8;
                for (var i = 0; i < 8; i++) {
                    me.cells[cell][i+1] = {
                        /*figure: null,
                        alias: cell + c,
                        horizontal: c,
                        vertical: cell,
                        coordinates: {},
                        size: {
                            w: 0,
                            h: 0
                        },
                        row: r,
                        column: c,*/
                        isBroken: function(color) {
                            return ($.inArray(this.alias, me._brokenCells().current(color, true)) > -1);
                        },
                        hasFigure: function() {
                            return ! ( ! this.figure);
                        }
                    };
                    //me.cellsCRIndex[r][c] = (cell + c);
                    //me.isFlipped ? ++c : --c;
                }
                ++r;
            });
            me._calcCellsCoordinates();
        },

        _calcCellsCoordinates: function() {
            var me = this,
            left = 0,
            top = 0,
            deltaH = me.figureHeight - me.cellSize;
            for (var cv=0; cv<8; cv++) {
                me.options.cellsCoordinates[cv] = [];
                for (var cg = 0; cg < 8; cg++) {
                    me.options.cellsCoordinates[cv][cg] = {
                        center: {
                            left: left + ((me.cellSize / 2) + (cg * me.cellSize)),
                            top: top + ((me.cellSize / 2) + (cv * me.cellSize)) - deltaH
                        },
                        offset: {
                            left: left + (cg * me.cellSize),
                            top: top + (cv * me.cellSize) - deltaH
                        }
                    }
                }
            }
            me._updateCellsConsist();
        },

        _updateCellsConsist: function() {

            var me = this,
            currentColumn = 0,
            deltaH = me.figureHeight - me.cellSize,
            h = me.cellSize + deltaH,
            y, i;
            if(me.isFlipped()) {
                currentColumn = 7;
            }
            $.each(me.cells, function(cell) {
                y = 1;
                for ((me.isFlipped() ? i = 0 : i = 7); (me.isFlipped() ? i < 8 : i > -1); (me.isFlipped() ? i++ : i--)) {
                    me.cells[cell][y].coordinates = me.options.cellsCoordinates[i][currentColumn];
                    me.cells[cell][y].size = {
                        w: me.cellSize,
                        h: me.cellSize + deltaH
                    };
                    me.cells[cell][y].alias = cell + y;
                    me.cells[cell][y].column = currentColumn + 1;
                    me.cells[cell][y].row = y;
                    me.cells[cell][y].vertical = cell;
                    me.cells[cell][y].horizontal = y;
                    me.cellsCRIndex[y][currentColumn + 1] = cell + y;
                    ++y;
                }
                me.isFlipped() ? currentColumn-- : currentColumn++;
            });
        },

        _getCellByCR: function(column, row) {
            var me = this;
            //console.log(me.cells)
            //console.log(me.cellsCRIndex)
            if(me.cellsCRIndex[column] && me.cellsCRIndex[column][row]) {
                return me.getCell(me.cellsCRIndex[column][row]);
            }
            return false;
        },

        _updateFiguresMoves: function() {
            var me = this;

            $.each(me._figures().getAll(), function(i, figure) {
                figure._calcMoves();
            });
            me._brokenCells().clear();
            $.each(me._figures().getAll(), function(i, figure) {
                figure._valMoves();
            });
            $.each(me._figures().getAll(), function(i, figure) {
                figure._afterValidateMoves();
                var fcell = figure.getCell();
                if(fcell.isBroken(figure.opponent())) {
                    figure.agressors = me._brokenCells().agressors(fcell.alias);
                    //figure._onBrokMyCell(me._brokenCells().agressors(fcell.alias));
                }
            });
        },

        /*_rollbackFiguresMoves: function() {
            var me = this;

            me.brokenCells.agressors = me.brokenCells.previousAgressors;
            me.brokenCells.curr = me.brokenCells.previousCurr;
            $.each(me._figures().getAll(), function(i, figure) {
                figure._rollbackMoves();
            });
        },*/

        _parseFEN: function(fen) {
            var me = this;
            fen = fen.split('/');
            var info = fen[7].split(' '),
            map = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
            y, x;
            fen[7] = info[0];
            $.each(map, function(index, row) {
                y = 7;
                x = 0;
                for (var i = 0; i < fen[index].length; i++) {
                    if( ! $.isNumeric(fen[index][i])) {
                        me.addFigure(map[x] + (y - index + 1), fen[index][i]);
                        x++;
                    }else{
                        x += parseInt(fen[index][i]);
                    }
                }
                y--;
            })
        },

        _parsePGN: function(pgn) {
            /*pgn = '[Event "IBM Kasparov vs. Deep Blue Rematch"]'+
                                '[Site "New York, NY USA"]'+
                                '[Date "1997.05.11"]'+
                                '[Round "6"]'+
                                '[White "Deep Blue"]'+
                                '[Black "Kasparov, Garry"]'+
                                '[Opening "Caro-Kann: 4...Nd7"]'+
                                '[ECO "B17"]'+
                                '[Result "1-0"]'+

                                '1.e4 c6 2.d4 d5 3.Nc3 dxe4 4.Nxe4 Nd7 5.Ng5 Ngf6 6.Bd3 e6 7.N1f3 h6 '+
                                '8.Nxe6 Qe7 9.O-O fxe6 10.Bg6+ {Deep Blue атакует} Kd8 {Каспаров встряхнул головой} '+
                                '11.Bf4 b5 12.a4 Bb7 13.Re1 Nd5 14.Bg3 Kc8 15.axb5 cxb5 16.Qd3 Bc6 '+
                                '17.Bf5 exf5 18.Rxe7 Bxe7 19.c4 1-0';*/
            var me = this,
            fen = /\[FEN \"(.*)\"]/im.exec(pgn),
            m = /1\.[^\d].*$/gim.exec(pgn),
            num = 1,
            moves = {},
            comment = ['', ''],
            w = 0, b = 1;
            m = m ? m[0].replace(/[\r\n]/g, ' ').split(/[0-9]{1,4}\./g) : [];

            fen = fen ? fen[1] : me._getOpts().FEN;

            $.each(m, function(i, move) {
                if(move) {
                    var p = /_\{([^\}]*)\}/g;
                    move = $.trim(move).replace(/\s\{/g, '_{').replace(p, function(dirty, val, index) {
                        comment[0] = index < 7 ? val : comment[0];
                        comment[1] = index >= 7 ? val : comment[1];
                        return '';
                    }).split(' ');
                    moves[num] = {
                        num: num,
                        w: {
                            move: move[w],
                            comment: comment[w]
                        },
                        b: {
                            move: move[b],
                            comment: comment[b]
                        }
                    }
                    num++;
                }

            });
            return fen;
        },

        _calcCellSize: function() {
            var cs = this.cellSize
            this.cellSize = this.boardSize / 8;
            this.figureHeight = (this.figureHeight * this.cellSize) / cs;
            return this.cellSize;
        },

        _getWrapper: function() {
            return this.wrapper;
        },

        figures: {
            b: {},
            w: {},
            all: {},
            count: 0
        },

        _figures: function() {
            var me = this,
            processor = {
                get: function(color, alias) {
                    if( ! alias) return me.figures[color];
                    return me.figures[color][alias];
                },
                getAll: function() {
                    return me.figures.all;
                },
                add: function(alias, figure) {
                    me.figures[figure.getColor()][alias] = figure;
                    me.figures.all[alias] = figure;
                    me.figures.count++;
                },
                remove: function(figure) {
                    delete me.figures[figure.getColor()][figure.alias];
                    delete me.figures.all[figure.alias];
                    me.figures.count--;
                },
                count: function() {
                    return me.figures.count;
                }
            };
            return processor;
        },

        placeFigures: function(fenOrPgn) {
            var me = this,
            fen;
            if( ! fenOrPgn) {
                fen = me._getOpts().FEN;
            }else{
                if(/\[/g.test(fenOrPgn)) {
                    fen = me._parsePGN(fenOrPgn);
                }
            }
            me._parseFEN(fen);
            me._getEl().trigger('figuresready', [me]);
        },

        addFigure: function(cell, figureType) {
            var me = this,
            color = 'w',
            cell = me.getCell(cell),
            el = $(document.createElement('div')),
            cls = figureType.toUpperCase();
            if (figureType == figureType.toLowerCase()) {
                color = 'b';
            }
            me._getWrapper().append(el);
            el[cls]({
                color: color,
                type: figureType
            });
            el[cls]('addToBoard', me, cell);
        },

        getCell: function(cellAlias) {
            var cell = cellAlias.substr(0,1),
            index = parseInt(cellAlias.substr(1,1));
            return this.cells[cell][index];
        },

        getCellByCoordinates: function(left, top) {
            var me = this,
            c = false;

            $.each(me.cells, function(alias, column) {
                $.each(column, function(index, cell) {
                    if(cell) {
                        var coords = cell.coordinates.center;
                        if(((coords.left - left) < me.cellSize && (coords.top - top) < me.cellSize) && ((coords.left - left) > 0 && (coords.top - top) > 0)) {
                            c = me.cells[alias][index];
                            return false;
                        }
                    }
                    if(c) return false;
                });
            });
            return c;
        },

        getFigures: function() {
            return $('.' + this.componentId + '-figure');
        },

        isFlipped: function() {
            return this.options.flip;
        }


    });
})(jQuery);