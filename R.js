(function($){
    $.widget('chessboard.R', $.chessboard.piece, {

        _initComponent: function() {
            var me = this;
            me.spriteCoordinates = {x: 240, y: me.spriteLine};
        },

        _calculateMoves: function() {
            var me = this,
            explorer = me.getBoard()._explorer(me),
            nb,
            moves = [
                explorer.vertical('front'),
                explorer.vertical('back'),
                explorer.horizontal('left'),
                explorer.horizontal('right')
            ];

            return moves;
        },

        _isCanReshuffle: function() {
            return this.movesCount === 0;
        }

    });
})(jQuery);