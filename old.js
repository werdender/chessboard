            (function($){

                $.extend($.fn, {
                    uniqueIdCounter: $.fn.uniqueIdCounter || 0,
                    uniqueId : function (prefix) {
                        ++$.fn.uniqueIdCounter;
                        return '' + (prefix || 'chessboard-') + $.fn.uniqueIdCounter;
                    }
                });

                $.fn.chessboard = function(cfg) {
                    var me = $.extend({
                        config: $.extend({
                            size: 480,
                            flip: false,
                            isResizable: true,
                            validateMoves: true,
                            enableSound: true,
                            board: false,
                            maxSize: 960,
                            minSize: 120,
                            blackColor: '#b58863',
                            whiteColor: '#f0d9b5',
                            figuresSprite: '/images/f.png',
                            defaultFEN: 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
                        }, cfg),
                        cellsCoords: [],
                        componentId: $.fn.uniqueId(),
                        cells: {a:[], b:[], c:[], d:[], e:[], f:[], g:[], h:[]},
                        cellsCRIndex: {1:[], 2:[], 3:[], 4:[], 5:[], 6:[], 7:[], 8:[]},
                        canvas: document.createElement('canvas'),

                        initComponent: function() {
                            me.boardSize = me.config.size;
                            me.cellSize = me.getCellSize();
                            me.canvas.width = me.boardSize;
                            me.canvas.height = me.boardSize;
                            me.canvas.id = me.componentId;
                            me.nextMoveColor = 'w';
                            me.currentMove = 0;
                            me.createBoard();
                            if(me.config.isResizable) {
                                me.on('resizestart', me.beforeResize);
                                me.on('resizestop', me.afterResize);
                                me.getCanvas().resizable({
                                    aspectRatio: 1/1,
                                    maxHeight: me.config.maxSize,
                                    maxWidth: me.config.maxSize,
                                    minHeight: me.config.minSize,
                                    minWidth: me.config.minSize,
                                    grid: 8,
                                    resize: me.onResize
                                });
                            }
                            if(me.config.enableSound) {
                                me.createSounds();
                            }
                            me.initCells();
                            me.on('move', me.onMove);
                            me.on('figureX', me.onFigureX);
                            me.on('figuresready', me.onFiguresReady);
                            me.on('reshuffle', me.onReshuffle);
                            me.on('check', me.onCheck);
                            //me.pos('rnbqkb1r/ppp2ppp/3p3n/8/2B1Pp2/2N2N2/PPPP2PP/R1BQ1K1R w kq -');
                            me.posByPGN();

                            return me;
                        },

                        initCells: function() {
                            $.each(me.cells, function(cell) {
                                var y = 1;
                                for (var i = 0; i < 8; i++) {
                                    me.cells[cell][y] = {
                                        figure: null,
                                        alias: cell + y,
                                        horizontal: cell,
                                        vertical: y,
                                        c: {},
                                        row: i + 1,
                                        column: y,
                                        isBroken: function(color, a, b) {
                                            return ($.inArray(this.alias, me.brokenCells().current(color, true)) > -1);
                                        }
                                    };
                                    me.cellsCRIndex[y][i + 1].push(cell + y);
                                    ++y;
                                }
                            });
                            me.calcCellsCoordinates();
                        },

                        isFlipped: function() {
                            return me.config.flip;
                        },

                        getCurrentMove: function() {
                            return me.currentMove;
                        },

                        updateCurrentMove: function(movedColor) {
                            me.nextMoveColor = movedColor == 'w' ? 'b' : 'w';
                            if(movedColor == 'w') {
                                me.currentMove++;
                            }else{
                                if(me.currentMove === 0) {
                                    me.currentMove++;
                                }
                            }
                        },

                        calcCellsCoordinates: function() {
                            var left = me.offset().left,
                                top = me.offset().top;
                            for (var cv=0; cv<8; cv++) {
                                me.cellsCoords[cv] = [];
                                for (var cg=0; cg<8; cg++) {
                                    me.cellsCoords[cv][cg] = {
                                        center: {
                                            left: left + ((me.cellSize / 2) + (cg * me.cellSize)),
                                            top: top + ((me.cellSize / 2) + (cv * me.cellSize))
                                        },
                                        offset: {
                                            left: left + (cg * me.cellSize),
                                            top: top + (cv * me.cellSize)
                                        }
                                    }
                                }
                            }
                            me.updateCells();
                        },

                        bCells: {
                            curr: {all: {}, b: {}, w: {}},
                            prev: {all: {}, b: {}, w: {}},
                            clear: function() {
                                me.bCells.prev = me.bCells.curr;
                                me.bCells.curr = {all: {}, b: {}, w: {}}
                            },
                            add: function(cellAlias, figure) {
                                if( ! me.bCells.curr.all[figure.componentId]) me.bCells.curr.all[figure.componentId] = [];
                                if( ! me.bCells.curr[figure.getColor()][figure.componentId]) me.bCells.curr[figure.getColor()][figure.componentId] = [];
                                me.bCells.curr.all[figure.componentId].push({cell: cellAlias, figure: figure});
                                me.bCells.curr[figure.getColor()][figure.componentId].push({cell: cellAlias, figure: figure});
                            },
                            get: function(actuality, color, asArrayOfAliases) {
                                var cells = [];
                                $.each(me.bCells[actuality][color], function(i, figure) {
                                    $.each(figure, function(i, bc) {
                                        if(asArrayOfAliases) {
                                            cells.push(bc.cell);
                                        }else{
                                            cells.push(bc);
                                        }
                                    });
                                });
                                return cells;
                            },
                            current: function(color, asArrayOfAliases) {
                                if( ! color) color = 'all';
                                return me.bCells.get('curr', color, asArrayOfAliases);
                            },
                            previous: function(color, asArrayOfAliases) {
                                if( ! color) color = 'all';
                                return me.bCells.get('prev', color, asArrayOfAliases);
                            },
                            del: function(type, figure) {
                                var brake = false;
                                $.each(me.bCells.curr[type], function(i, f) {
                                    $.each(f, function(y, bc) {
                                        if(bc.figure.componentId == figure.componentId) {
                                            delete me.bCells.curr[type][figure.componentId];
                                            brake = true;
                                            return false;
                                        }
                                    });
                                    if(brake) return false;
                                });
                            },
                            remove: function(figure) {
                                me.bCells.del('all', figure);
                                me.bCells.del(figure.getColor(), figure)
                            }
                        },

                        brokenCells: function() {
                            return me.bCells;
                        },

                        updateCells: function() {
                            var currentColumn = 0,
                                i;
                            if(me.config.flip) {
                                currentColumn = 7;
                            }
                            $.each(me.cells, function(cell) {
                                var y = 1;
                                for ((me.isFlipped() ? i = 0 : i = 7); (me.isFlipped() ? i < 8 : i > -1); (me.isFlipped() ? i++ : i--)) {
                                    me.cells[cell][y].c = me.cellsCoords[i][currentColumn];
                                    me.cells[cell][y].alias = cell + y;
                                    me.cells[cell][y].column = currentColumn + 1;
                                    me.cells[cell][y].row = i + 1;
                                    me.cells[cell][y].horizontal = cell;
                                    me.cells[cell][y].vertical = y;
                                    ++y;
                                }
                                me.isFlipped() ? currentColumn-- : currentColumn++;
                            });
                        },

                        getCell: function(cellAlias) {
                            var cell = cellAlias.substr(0,1),
                                index = parseInt(cellAlias.substr(1,1));
                            return me.cells[cell][index];
                        },

                        getCellByCoordinates: function(left, top) {
                            var c = false;
                            $.each(me.cells, function(alias, column) {
                                $.each(column, function(index, cell) {
                                    if(cell) {
                                        var coords = cell.c.center;
                                        if(((coords.left - left) < me.cellSize && (coords.top - top) < me.cellSize) && ((coords.left - left) > 0 && (coords.top - top) > 0)) {
                                            c = me.cells[alias][index];
                                            return false;
                                        }
                                    }
                                    if(c) return false;
                                });
                            });
                            return c;
                        },

                        getCellByCR: function(column, row) {
                            var c = false;
                            $.each(me.cells, function(alias, r) {
                                $.each(r, function(index, cell) {
                                    if(cell) {
                                        if(cell.row == row && cell.column == column) {
                                            c = me.cells[alias][index];
                                            return false;
                                        }
                                    }
                                    if(c) return false;
                                });
                            });
                            return c;
                        },

                        getEl: function() {
                            return me[0];
                        },

                        getCanvas: function() {
                            if(me.cEl) {
                                return me.cEl;
                            }
                            me.cEl = $('#' + me.canvas.id);
                            return me.cEl;
                        },

                        onResize: function(event, ui) {
                            me.boardSize = ui.size.width;
                            me.cellSize = me.getCellSize();
                            me.calcCellsCoordinates();
                        },

                        getCellSize: function() {
                            return me.boardSize / 8;
                        },

                        toggleFlip: function() {
                            me.config.flip = ! me.config.flip;
                            me.calcCellsCoordinates();
                        },

                        createBoard: function() {
                            var ctx = me.canvas.getContext('2d');
                            if( ! me.config.board) {
                                ctx.fillStyle = me.config.blackColor; // draw cells
                                ctx.fillRect(0, 0, me.boardSize, me.boardSize);
                                for (var i=0; i<8; i+=2) {
                                    for (var j=0; j<8; j+=2) {
                                        ctx.fillStyle = me.config.whiteColor;
                                        ctx.fillRect(0+i*me.cellSize, 0+j*me.cellSize, me.cellSize, me.cellSize);
                                        ctx.fillRect(0+(i+1)*me.cellSize, 0+(j+1)*me.cellSize, me.cellSize, me.cellSize);
                                    }
                                }
                                ctx.strokeStyle = me.config.blackColor; // draw board frame
                                ctx.strokeRect(0, 0, me.boardSize, me.boardSize);
                            }else{
                                var board = new Image();
                                board.src = me.config.board;
                                board.onload = function() {
                                    ctx.drawImage(board, 0, 0, me.boardSize, me.boardSize);
                                }
                            }
                            me.getEl().appendChild(me.canvas);
                        },

                        createFigure: function(cell, figure) {
                            var color = 'w',
                                cell = me.getCell(cell),
                                el = document.createElement('div');
                            if (figure == figure.toLowerCase()) {
                                color = 'b';
                            }
                            me.getEl().appendChild(el);
                            cell.figure = $(el);
                            return cell.figure.figure({
                                color: color,
                                type: figure,
                                board: me,
                                cell: cell
                            });
                        },

                        createSounds: function() {
                            me.audio.add('X', ['sounds/piece_x.ogg', 'sounds/piece_x.mp3']);
                            me.audio.add('move', ['sounds/piece_move.ogg', 'sounds/piece_move.mp3']);
                            me.audio.add('game_start', ['sounds/game_start.ogg', 'sounds/game_start.mp3']);
                            me.audio.add('game_over', ['sounds/game_over.ogg', 'sounds/game_over.mp3']);
                            me.audio.add('check', ['sounds/check.ogg', 'sounds/check.mp3']);
                            me.audio.add('reshuffle', ['sounds/reshuffle.ogg', 'sounds/reshuffle.mp3']);
                        },

                        audio: {
                            play: function(name) {
                                if( ! me.config.enableSound) {
                                    return false;
                                }
                                me.audio[name].play();
                            },

                            add: function(name, urls) {
                                var a = document.createElement('audio'),
                                nocache = '_ds='+new Date().getTime(),
                                sources = [document.createElement('source'), document.createElement('source')];
                                a.preload = 'auto';
                                a.id = me.componentId + '-audio-' + name;
                                sources[0].src = urls[0] + '?' + nocache;
                                sources[1].src = urls[1] + '?' + nocache;
                                a.appendChild(sources[0]);
                                a.appendChild(sources[1]);
                                document.body.appendChild(a);
                                me.audio[name] = a;
                            }
                        },

                        posByFEN: function(fen) {
                            fen = fen.split('/');
                            var info = fen[7].split(' '),
                            map = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
                            y, x;
                            fen[7] = info[0];
                            $.each(map, function(index, row) {
                                y = 7;
                                x = 0;
                                for (var i = 0; i < fen[index].length; i++) {
                                    if( ! $.isNumeric(fen[index][i])) {
                                        me.createFigure(map[x] + (y-index+1), fen[index][i]);
                                        x++;
                                    }else{
                                        x += parseInt(fen[index][i]);
                                    }
                                }
                                y--;
                            })
                        },

                        posByPGN: function(pgn) {
                            /*pgn = '[Event "IBM Kasparov vs. Deep Blue Rematch"]'+
                                '[Site "New York, NY USA"]'+
                                '[Date "1997.05.11"]'+
                                '[Round "6"]'+
                                '[White "Deep Blue"]'+
                                '[Black "Kasparov, Garry"]'+
                                '[Opening "Caro-Kann: 4...Nd7"]'+
                                '[ECO "B17"]'+
                                '[Result "1-0"]'+

                                '1.e4 c6 2.d4 d5 3.Nc3 dxe4 4.Nxe4 Nd7 5.Ng5 Ngf6 6.Bd3 e6 7.N1f3 h6 '+
                                '8.Nxe6 Qe7 9.O-O fxe6 10.Bg6+ {Deep Blue атакует} Kd8 {Каспаров встряхнул головой} '+
                                '11.Bf4 b5 12.a4 Bb7 13.Re1 Nd5 14.Bg3 Kc8 15.axb5 cxb5 16.Qd3 Bc6 '+
                                '17.Bf5 exf5 18.Rxe7 Bxe7 19.c4 1-0';*/
                            pgn = '[Date "????.??.??"]'+
                                '[Result "*"]'+
                                '[FEN "rnbqkb1r/ppp2ppp/3p3n/8/2B1Pp2/2N2N2/PPPP2PP/R1BQ1K1R w kq -"]'+

                                '1.Nd5 c6 2.Nb4 d5 '+
                                '*';
                            var fen = /\[FEN \"(.*)\"]/im.exec(pgn),
                                m = /1\.[^\d].*$/gim.exec(pgn),
                                num = 1,
                                moves = {},
                                comment = ['', ''],
                                w = 0, b = 1;
                            m = m ? m[0].replace(/[\r\n]/g, ' ').split(/[0-9]{1,4}\./g) : [];
                            fen = fen ? fen[1] : me.config.defaultFEN;

                            $.each(m, function(i, move) {
                                if(move) {
                                    var p = /_\{([^\}]*)\}/g;
                                    move = $.trim(move).replace(/\s\{/g, '_{').replace(p, function(dirty, val, index) {
                                        comment[0] = index < 7 ? val : comment[0];
                                        comment[1] = index >= 7 ? val : comment[1];
                                        return '';
                                    }).split(' ');
                                    moves[num] = {
                                        num: num,
                                        w: {
                                            move: move[w],
                                            comment: comment[w]
                                        },
                                        b: {
                                            move: move[b],
                                            comment: comment[b]
                                        }
                                    }
                                    num++;
                                }

                            });
                            //console.log(moves);
                            me.pos(fen);
                        },

                        pos: function(fen) {
                            if( ! fen) {
                                fen = me.config.defaultFEN;
                            }
                            me.posByFEN(fen);
                            me.trigger('figuresready', [me]);
                        },

                        figures: {
                            get: function(color, alias) {
                                if( ! alias) return me.figures.figures[color];
                                return me.figures.figures[color][alias];
                            },
                            add: function(alias, figure) {
                                me.figures.figures[figure.getColor()][alias] = figure;
                            },
                            remove: function(figure) {
                                me.figures.figures[figure.getColor()][figure.alias] = null;
                            },
                            figures: {
                                b: {},
                                w: {}
                            }
                        },

                        /*getKing: function(color) {
                            return me[color + 'K'];
                        },*/

                        getKingRook: function(direction) {

                        },

                        getQueenRook: function(direction) {

                        },

                        beforeResize: function() {
                            me.getFigures().hide();
                        },

                        afterResize: function(w, ui) {
                            me.getFigures().show();
                        },

                        getFigures: function() {
                            return $('.' + me.componentId + '-figure');
                        },

                        onMove: function(event, board, figure, cellFrom, cellTo) {
                            me.audio.play('move');
                        },

                        onFigureX: function(event, figure) {
                            me.audio.play('X');
                        },

                        onFiguresReady: function(event, board) {
                            me.audio.play('game_start');
                            //console.log(me.figures.get('w', 'King').canOO())
                        },

                        onCheck: function(event, king, aggressor) {
                            me.audio.play('check');
                        },

                        onGameOver: function(event, board, color) {
                            me.audio.play('game_over');
                        },

                        onReshuffle: function(event, type, king, rook, color) {
                            me.audio.play('reshuffle');
                        }

                    }, this);
                    return me.initComponent();
                }

                $.fn.figure = function(cfg) {
                    var me = $.extend({
                        config: $.extend({
                            color: 'w',
                            type: 'p',
                            board: $('#chessboard-1')
                        }, cfg),
                        movesCount: 0,
                        legalMoves: [],
                        sprite: new Image(),

                        initComponent: function() {
                            me.config.cell.figure = me;
                            me.attr('class', me.getBoard().componentId + '-figure');
                            me.addClass(me.getBoard().componentId + '-' + me.getType() + '-' + me.getColor());
                            me.css({
                                position: 'absolute',
                                cursor: 'pointer',
                                border: 0,
                                left: me.getCell().c.offset.left+'px',
                                top: me.getCell().c.offset.top+'px',
                                width: me.getBoard().cellSize,
                                height: me.getBoard().cellSize
                            });
                            me.html('');
                            me.sprite.src = me.getBoard().config.figuresSprite;
                            me.componentId = $.fn.uniqueId(me.getColor() + '-' + me.getType() + '-');


                            if(me.getColor() == 'w') {
                                me.spriteLine = 0;
                            }else{
                                me.spriteLine = 120;
                            }
                            me.addToBoard();
                            me.sprite.onload = function() {
                                me.drawImage();
                                me.getCell().figure = me;
                            }
                            me.config.board.on('resize', me.update);
                            me.config.board.on('resizestop', me.update);
                            me.on('dragstop', me.onDragStop);
                            me.config.board.on('move', me.onMove);
                            me.config.board.on('figuresready', me.calcMoves);
                            return me;
                        },

                        addToBoard: function() {
                            var board = me.getBoard();
                            switch (me.getType()) {
                                case 'K':
                                    me.alias = 'King';
                                    break;
                                case 'Q':
                                    me.alias = 'Queen';
                                    break;
                                case 'R':
                                    if(me.getColor() == 'w') {
                                        if(me.getCell().alias == 'h1') {
                                            me.alias = 'KingRook';
                                        }else{
                                            me.alias = 'QueenRook';
                                        }
                                    }else{
                                        if(me.getCell().alias == 'h8') {
                                            me.alias = 'KingRook';
                                        }else{
                                            me.alias = 'QueenRook';
                                        }
                                    }
                                    break;
                                case 'N':
                                    if(me.getColor() == 'w') {
                                        if(me.getCell().alias == 'g1') {
                                            me.alias = 'KingKnight';
                                        }else{
                                            me.alias = 'QueenKnight';
                                        }
                                    }else{
                                        if(me.getCell().alias == 'g8') {
                                            me.alias = 'KingKnight';
                                        }else{
                                            me.alias = 'QueenKnight';
                                        }
                                    }
                                    break;
                                case 'B':
                                    if(me.getColor() == 'w') {
                                        if(me.getCell().alias == 'f1') {
                                            me.alias = 'whiteBishop';
                                        }else{
                                            me.alias = 'blackBishop';
                                        }
                                    }else{
                                        if(me.getCell().alias == 'c8') {
                                            me.alias = 'whiteBishop';
                                        }else{
                                            me.alias = 'blackBishop';
                                        }
                                    }
                                    break;
                                default:
                                    me.alias = me.componentId;
                                    break;
                            }
                            board.figures.add(me.alias, me)
                        },

                        getSpriteCoordinates: function() {
                            me.config.type = me.config.type.toUpperCase();
                            if(me.config.type == 'P') {
                                me.config.type = 'p';
                            }
                            switch (me.getType()) {
                                case 'K': return [0, me.spriteLine];
                                case 'Q': return [120, me.spriteLine];
                                case 'R': return [240, me.spriteLine];
                                case 'N': return [360, me.spriteLine];
                                case 'B': return [480, me.spriteLine];
                                default: return [600, me.spriteLine];
                            }
                        },

                        hasMoves: function() {
                            return me.movesCount > 0;
                        },

                        drawImage: function() {
                            var c = me.getSpriteCoordinates();
                            if(me.canvas) {
                                me.canvas.ctx.clearRect(0, 0, 500, 500);
                            }else{
                                me.canvas = document.createElement('canvas');
                                $(me.canvas).attr('class', me.getBoard().componentId + '-figure-canvas')
                                me.canvas.id = me.componentId+'-canvas';
                                me.canvas.ctx = me.canvas.getContext('2d');
                                me.getCell().figure[0].appendChild(me.canvas);
                                me.getCell().figure.draggable({containment: '#' + me.getBoard().componentId, scroll: false});
                            }
                            me.canvas.width  = me.getBoard().cellSize;
                            me.canvas.height = me.getBoard().cellSize;
                            me.canvas.ctx.drawImage(me.sprite, c[0], c[1], 120, 120, 0, 0, me.getBoard().cellSize, me.getBoard().cellSize);
                        },

                        update: function() {
                             me.css({
                                left: me.getCell().c.offset.left+'px',
                                top: me.getCell().c.offset.top+'px',
                                width: me.getBoard().cellSize,
                                height: me.getBoard().cellSize
                            });
                            me.drawImage();
                            /*$(me.canvas).css({
                                width: me.getBoard().cellSize,
                                height: me.getBoard().cellSize
                            });*/
                        },

                        getCell: function() {
                            return me.config.cell;
                        },

                        isCheck: function(cell) {
                            var brokenCells = me.getBoard().brokenCells().current(me.opponent(), true);
                            if(me.getCell().isBroken(me.opponent())) {
                                return true;
                            }
                            return false;
                        },

                        isCheckOnMoveToCell: function(cell) {
                            if(me.getType() == 'K' && cell.isBroken(me.opponent())) {
                                return true;
                            }
                            var virtualMoves = [],
                                king = me.getBoard().figures.get(me.getColor(), 'King'),
                                virtualCell = $.extend({}, cell);
                            virtualCell.figure = me;
                            $.each(me.getBoard().figures.get(me.opponent()), function(i, figure) {
                                if(figure) {
                                    virtualMoves = virtualMoves.concat(figure.calcMoves(true, virtualCell));
                                }
                            });
                            virtualCell = null;
                            if($.inArray(king.getCell().alias, virtualMoves) > -1) {
                                return true;
                            }
                            return false;
                        },

                        setCell: function(cell, reshuffle) {
                            if((cell && me.isValidMove(cell)) || reshuffle) {
                                var king = me.getBoard().figures.get(me.getColor(), 'King');
                                if(me.isCheckOnMoveToCell(cell)) {
                                    return false;
                                }
                                if(cell.figure) {
                                    cell.figure.x();
                                }
                                me.config.cell.figure = null;
                                me.config.cell = cell;
                                me.config.cell.figure = me;
                                me.movesCount++;
                                return true;
                            }
                            return false;
                        },

                        move: function(cell, reshuffle) {
                            if(typeof cell == 'string') {
                                cell = me.getBoard().getCell(cell);
                            }
                            me.setCell(cell, reshuffle);
                            me.animate({
                               left: me.getCell().c.offset.left+'px',
                               top: me.getCell().c.offset.top+'px'
                           });
                        },

                        x: function() {
                            me.getBoard().brokenCells().remove(me);
                            me.getBoard().figures.remove(me);
                            me.getCell().figure = null;
                            me.getBoard().trigger('figureX', [me]);
                            me.remove();
                            delete me;
                        },

                        getBoard: function() {
                            return me.config.board;
                        },

                        OO: function() {
                            var rook = me.getBoard().figures.get(me.getColor(), 'KingRook'),
                                cellFrom = rook.getCell(),
                                cell;
                            if(me.getColor() == 'w') {
                                cell = me.getBoard().getCell('f1');
                            }else{
                                cell = me.getBoard().getCell('f8');
                            }
                            rook.move(cell, true);
                            me.getBoard().trigger('reshuffle', ['O-O', me, rook, me.getColor()]);
                            me.getBoard().trigger('move', [me.getBoard(), rook, cellFrom, cell]);
                        },

                        OOO: function() {
                            var rook = me.getBoard().figures.get(me.getColor(), 'QueenRook'),
                                cellFrom = rook.getCell(),
                                cell;
                            if(me.getColor() == 'w') {
                                cell = me.getBoard().getCell('d1');
                            }else{
                                cell = me.getBoard().getCell('d8');
                            }
                            rook.move(cell, true);
                            me.getBoard().trigger('reshuffle', ['O-O-O', me, rook, me.getColor()]);
                            me.getBoard().trigger('move', [me.getBoard(), rook, cellFrom, cell]);
                        },

                        canReshuffle: function(type) {
                            if(me.hasMoves()) {
                                return false;
                            }
                            if(me.getColor() == 'w' && me.getCell().alias != 'e1') return false;
                            if(me.getColor() == 'b' && me.getCell().alias != 'e8') return false;
                            var rook = type == 'O-O-O' ?  me.getBoard().figures.get(me.getColor(), 'QueenRook') : me.getBoard().figures.get(me.getColor(), 'KingRook'),
                                count = type == 'O-O-O' ? 3 : 2,
                                direction = me.getColor() == 'w' ? 'right' : 'left',
                                OObreak = false;
                            if(type == 'O-O-O') {
                                direction = me.getColor() == 'w' ? 'left' : 'right';
                            }
                            var steps = me.utils().horizontal(direction, count);
                            if( ! rook || rook.hasMoves()) {
                                return false;
                            }
                            $.each(steps, function(i, step) {
                                var cell = me.getBoard().getCellByCR(step.c, step.r);
                                if(cell && cell.figure) OObreak = true;
                            });
                            return ( ! OObreak);
                        },

                        canOO: function() {
                            return me.canReshuffle();
                        },

                        canOOO: function() {
                            return me.canReshuffle('O-O-O');
                        },


                        reincarnate: function() {

                        },

                        bCells: {
                            curr: [],
                            prev: [],
                            clear: function() {
                                me.bCells.prev = me.bCells.curr;
                                me.bCells.curr = []
                            },
                            add: function(cellAlias) {
                                me.bCells.curr.push(cellAlias);
                            },
                            current: function() {
                                return me.bCells.curr;
                            },
                            previous: function() {
                                return me.bCells.prev;
                            }
                        },

                        brokenCells: function() {
                            return me.bCells;
                        },

                        virtualMovesBuffer: [],

                        isValidMove: function(cell) {
                            if( ! me.getBoard().config.validateMoves) {
                                return true;
                            }
                            cell = cell.alias ? cell.alias : cell;
                            return $.inArray(cell, me.legalMoves) > -1;
                        },

                        utils: function() {
                            var cell = me.currentCell,
                                column = cell.column,
                                row = cell.row,
                                board = me.getBoard(),
                                color = me.getColor();
                            var u = {
                                addMoves: function(moves, virtual){
                                    var cell;
                                    $.each(moves, function(i, m) {
                                        cell = board.getCellByCR(m.c, m.r);
                                        if(cell) {
                                            var broken = m.broken === false ? false : true;
                                            if( ! virtual) {
                                                me.legalMoves.push(cell.alias);
                                                if(broken) {
                                                    me.getBoard().brokenCells().add(cell.alias, me);
                                                    me.brokenCells().add(cell.alias);
                                                }
                                            }else{
                                                me.virtualMovesBuffer.push(cell.alias);
                                            }
                                        }
                                    });
                                },
                                stepV: function(n, direction) {
                                    var res;
                                    if(me.getColor() == 'w') {
                                        if(me.getBoard().isFlipped()) {
                                            res = direction == 'front' ? row + n : row - n;
                                        }else{
                                            res = direction == 'front' ? row - n : row + n;
                                        }
                                    }else{
                                        if(me.getBoard().isFlipped()) {
                                            res = direction == 'front' ? row - n : row + n;
                                        }else{
                                            res = direction == 'front' ? row + n : row - n;
                                        }
                                    }
                                    return res;
                                },
                                stepH: function(n, direction){
                                    var res;
                                    if(me.getColor() == 'w') {
                                        if(me.getBoard().isFlipped()) {
                                            res = direction == 'right' ? column - n : column + n;
                                        }else{
                                            res = direction == 'right' ? column + n : column - n;
                                        }
                                    }else{
                                        if(me.getBoard().isFlipped()) {
                                            res = direction == 'right' ? column + n : column - n;
                                        }else{
                                            res = direction == 'right' ? column - n : column + n;
                                        }
                                    }
                                    return res;
                                },
                                hasFigure: function(step) {
                                    if(board.getCellByCR(step.c, step.r).figure) {
                                        return true;
                                    }
                                    return false;
                                },
                                canX: function(step) {
                                    if( ! u.hasFigure(step)) {
                                        return false;
                                    }
                                    return (board.getCellByCR(step.c, step.r).figure.getColor() != me.getColor());
                                },
                                diagonal: function(vDirection, hDirection, count) {
                                    var res = [];
                                    count = count ? (count + 1) : 9;
                                    for(var i = 1; i < count; i++) {
                                        res.push({c: u.stepH(i, hDirection), r: u.stepV(i, vDirection)})
                                    }
                                    return res;
                                },
                                vertical: function(direction, count) {
                                    var res = [];
                                    count = count ? (count + 1) : 9;
                                    for(var i = 1; i < count; i++) {
                                        res.push({c: column, r: u.stepV(i, direction)})
                                    }
                                    return res;
                                },
                                horizontal: function(direction, count) {
                                    var res = [];
                                    count = count ? (count + 1) : 9;
                                    for(var i = 1; i < count; i++) {
                                        res.push({c: u.stepH(i, direction), r: row})
                                    }
                                    return res;
                                },
                                getValidSteps: function(steps, noBrokenOnly) {
                                    var lm = [];
                                    $.each(steps, function(i, s) {
                                        var stepBrake = false;
                                        $.each(s, function(y, step) {
                                            if( ! board.getCellByCR(step.c, step.r)) return true;
                                            if(noBrokenOnly) {
                                                if(board.getCellByCR(step.c, step.r).isBroken(me.opponent())) {
                                                    return false;
                                                }
                                            }
                                            if( ! u.hasFigure(step) && ! stepBrake) {
                                                lm.push(step);
                                            }else if(u.canX(step) && ! stepBrake) {
                                                lm.push(step);
                                                stepBrake = true;
                                            }else{
                                                stepBrake = true;
                                            }
                                        });
                                    });
                                    return lm;
                                }
                            };
                            return u;
                        },

                        calcMoves: function(virtual, virtualCell) {
                            virtual = virtual === true ? true : false;
                            me.virtualMovesBuffer = [];
                            if(virtual) {
                                var realCell = me.getBoard().getCell(virtualCell.alias);
                                me.getBoard().cells[virtualCell.horizontal][virtualCell.vertical] = virtualCell;
                            }
                            var cell = me.getCell(),
                                column = cell.column,
                                row = cell.row,
                                board = me.getBoard(),
                                color = me.config.color,
                                lm = [];
                            me.legalMoves = [];
                            me.currentCell = cell;
                            var utils = me.utils();
                            switch (me.getType()) {
                                case 'K': (function() {
                                    var steps = [
                                        utils.diagonal('front', 'left', 1),
                                        utils.diagonal('front', 'right', 1),
                                        utils.diagonal('back', 'left', 1),
                                        utils.diagonal('back', 'right', 1),
                                        utils.vertical('front', 1),
                                        utils.vertical('back', 1),
                                        utils.horizontal('left', 1),
                                        utils.horizontal('right', 1)
                                    ];
                                    utils.addMoves(utils.getValidSteps(steps, true), virtual);
                                    if(me.canOO()) {
                                        var direction = me.getColor() == 'w' ? 'right' : 'left';
                                        steps = [
                                            {c: utils.stepH(2, direction), r: utils.stepV(0, 'front')}
                                        ];
                                        utils.addMoves(steps, virtual);
                                    }
                                    if(me.canOOO()) {
                                        var direction = me.getColor() == 'w' ? 'left' : 'right';
                                        steps = [
                                            {c: utils.stepH(2, direction), r: utils.stepV(0, 'front')}
                                        ];
                                        utils.addMoves(steps, virtual);
                                    }
                                })();
                                break;
                                case 'Q': (function() {
                                    var steps = [
                                        utils.diagonal('front', 'left'),
                                        utils.diagonal('front', 'right'),
                                        utils.diagonal('back', 'left'),
                                        utils.diagonal('back', 'right'),
                                        utils.vertical('front'),
                                        utils.vertical('back'),
                                        utils.horizontal('left'),
                                        utils.horizontal('right')
                                    ];
                                    utils.addMoves(utils.getValidSteps(steps), virtual);
                                })();
                                break;
                                case 'R': (function() {
                                    var steps = [
                                        utils.vertical('front'),
                                        utils.vertical('back'),
                                        utils.horizontal('left'),
                                        utils.horizontal('right')
                                    ];
                                    utils.addMoves(utils.getValidSteps(steps), virtual);
                                })();
                                break;
                                case 'N': (function() {
                                    var steps = [
                                        {c: utils.stepH(1, 'right'), r: utils.stepV(2, 'front')},
                                        {c: utils.stepH(1, 'left'), r: utils.stepV(2, 'front')},
                                        {c: utils.stepH(2, 'right'), r: utils.stepV(1, 'front')},
                                        {c: utils.stepH(2, 'left'), r: utils.stepV(1, 'front')},
                                        {c: utils.stepH(1, 'right'), r: utils.stepV(2, 'back')},
                                        {c: utils.stepH(1, 'left'), r: utils.stepV(2, 'back')},
                                        {c: utils.stepH(2, 'right'), r: utils.stepV(1, 'back')},
                                        {c: utils.stepH(2, 'left'), r: utils.stepV(1, 'back')}
                                    ];
                                    $.each(steps, function(i, step) {
                                        if( ! utils.hasFigure(step) || utils.canX(step)) {
                                            lm.push(step);
                                        }
                                    });
                                    utils.addMoves(lm, virtual);
                                })();
                                break;
                                case 'B': (function() {
                                    var steps = [
                                        utils.diagonal('front', 'left'),
                                        utils.diagonal('front', 'right'),
                                        utils.diagonal('back', 'left'),
                                        utils.diagonal('back', 'right')
                                    ];
                                    utils.addMoves(utils.getValidSteps(steps), virtual);
                                })();
                                break;
                                default: (function() { // pawn
                                    var step = {c: column, r: utils.stepV(1, 'front'), broken: false}, // move on 1 cell to front
                                        stepBrake = false,
                                        onSelfCell = me.getColor() == 'w' ? (me.getCell().vertical == 2) : (me.getCell().vertical == 7);
                                    if( ! utils.hasFigure(step)) {
                                        lm.push(step);
                                    }else{
                                        stepBrake = true;
                                    }
                                    step = {c: column, r: utils.stepV(2, 'front'), broken: false}; // fist move on 2 cells to front
                                    if( ! utils.hasFigure(step) && onSelfCell && ! stepBrake) {
                                        lm.push(step);
                                    }
                                    step = {c: utils.stepH(1, 'right'), r: utils.stepV(1, 'front')}; // x figures
                                    if(utils.canX(step)) {
                                        lm.push(step);
                                    }
                                    step = {c: utils.stepH(1, 'left'), r: utils.stepV(1, 'front')};
                                    if(utils.canX(step)) {
                                        lm.push(step);
                                    }
                                    var passedPawnsCells = [ // x passed pawns
                                        {c: utils.stepH(1, 'left'), r: utils.stepV(0, 'front')},
                                        {c: utils.stepH(1, 'right'), r: utils.stepV(0, 'front')}
                                    ], ppc, pawn;
                                    $.each(passedPawnsCells, function(i, cell) {
                                        ppc = board.getCellByCR(cell.c, cell.r);
                                        if(ppc && ppc.figure && ppc.figure.getType() == 'P') {
                                            pawn = ppc.figure;
                                            if(pawn.isPassedPawn && pawn.passedMove === board.getCurrentMove()) {
                                                var step = {c: pawn.getCell().column, r: utils.stepV(1, 'front')},
                                                    passedXCell = board.getCellByCR(step.c, step.r).alias;
                                                lm.push(step);
                                                me.onMoveTo(passedXCell, function() {
                                                    pawn.x();
                                                });
                                            }
                                        }
                                    });
                                    utils.addMoves(lm, virtual);
                                })();
                            }
                            if(virtual) {
                                me.getBoard().cells[virtualCell.horizontal][virtualCell.vertical] = realCell;
                            }
                            return me.virtualMovesBuffer;
                        },

                        onMoveTo: function(cellAlias, fn) {
                            var moveFn = function(event, board, figure, cellFrom, cellTo) {
                                if(cellTo.alias == cellAlias) {
                                    fn.call();
                                }
                                me.getBoard().off('move', moveFn);
                            }
                            me.getBoard().on('move', moveFn);
                        },

                        getType: function() {
                            return me.config.type.toUpperCase();
                        },

                        getColor: function() {
                            if(!me) console.log(arguments.callee.toSource())
                            return me.config.color;
                        },

                        opponent: function() {
                            return me.getColor() == 'w' ? 'b' : 'w';
                        },

                        onDragStop: function(event, ui) {
                            var cellFrom = me.getCell(),
                                cellTo = me.getBoard().getCellByCoordinates(ui.position.left, ui.position.top);
                            if(me.setCell(cellTo)) {
                                me.getBoard().updateCurrentMove(me.getColor());
                                if(me.getType() == 'K') { // is reshuffle
                                    var delta = Math.abs(cellFrom.column - cellTo.column);
                                    if(delta === 2) {
                                        if(me.getCell().alias == 'g8' || me.getCell().alias == 'g1') {
                                            me.OO();
                                        }
                                        if(me.getCell().alias == 'c8' || me.getCell().alias == 'c1') {
                                            me.OOO();
                                        }
                                    }
                                }
                                if(me.getType() == 'P') { // is passed pawn
                                    var delta = Math.abs(cellFrom.row - cellTo.row);
                                    if(delta === 2) {
                                        me.isPassedPawn = true;
                                        me.passedMove = me.getBoard().getCurrentMove();
                                    }else{
                                        me.isPassedPawn = false;
                                    }
                                }
                                me.brokenCells().clear();
                                me.getBoard().brokenCells().clear();
                                me.getBoard().trigger('move', [me.getBoard(), me, cellFrom, cellTo]);
                            };
                            me.update();
                        },

                        isEqual: function(figure) {
                            return figure.componentId == me.componentId;
                        },

                        onMove: function(event, board, figure, cellFrom, cellTo) {
                            if( ! me) {
                                return;
                            }
                            me.calcMoves();
                            var king = me.getBoard().figures.get(me.opponent(), 'King'),
                                kingCellAlias = king.getCell().alias;
                            if(king.getCell().isBroken(king.opponent())) {
                                me.getBoard().trigger('check', [king, me]);
                            }
                        }

                    }, this);
                    return me.initComponent();
                }

            })(jQuery);