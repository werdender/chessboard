(function($){
    $.widget('chessboard.piece', {

        options: {
            type: 'p',
            color: 'w'
        },

        possibleMoves: [],
        validMoves: [],
        movesBrokenCells: {},
        movesCount: 0,
        spriteCoordinates: {
            x: 0,
            y: 0
        },
        board: null,
        cell: null,
        alias: null,

        _create: function () {
            var me = this;
        },

        addToBoard: function(board, cell) {
            var me = this;
            me.board = board;
            me.cell = cell;
            me.cell.figure = me;
            me._getEl().attr('class', me.getBoard().componentId + '-figure');
            me._getEl().addClass(me.getBoard().componentId + '-' + me.getType() + '-' + me.getColor());
            me._getEl().uniqueId();
            me._getEl().html('');
            me.componentId = me._getEl().attr('id');
            me._getEl().addClass('chessboard-figure-' + me.componentId);
            me._getEl().addClass('chessboard-figure-' + me.getColor());
            //

            if(me.getColor() == 'w') {
                me.spriteLine = 0;
            }else{
                me.spriteLine = 120;
            }

            me._initComponent();
            me._render();

            me.getCell().figure = me;
            me.alias = me.alias || me.componentId;
            me.getBoard()._figures().add(me.alias, me);
            //me.getBoard()._getEl().on('resize', $.proxy(me._updateEl, me));
            me.getBoard()._getEl().on('resizestop', $.proxy(me._updateEl, me));
            //me.getBoard()._getEl().on('figuremove', $.proxy(me._onBoardFigureMove, me));
            me.getBoard()._getEl().on('figuresready', $.proxy(me._onBoardFiguresReady, me));
            me._getEl().on('dragstop', $.proxy(me._onDragStop, me));
            me._brokenCells().clear();
        },

        _initComponent: function() {},

        _destroy: function () {
            //console.log(this.bindings)
            var me = this,
            element = document.getElementById(this.componentId);

            me.getCell().figure = null;

            //me.getBoard()._brokenCells().remove(me);
            me.getBoard()._figures().remove(me);
            element.parentNode.removeChild(element);
        },


        _setOption: function (key, value) {
            this._super( "_setOption", key, value );
        },

        _render: function() {
            var me = this;

            if( ! me._getEl()) return;

            if(me.canvas) {
                me.canvas.ctx.clearRect(0, 0, 500, 500);
            }else{
                me.canvas = document.createElement('canvas');
                $(me.canvas).attr('class', me.getBoard().componentId + '-figure-canvas')
                me.canvas.id = me.componentId+'-canvas';
                me.canvas.ctx = me.canvas.getContext('2d');
                me._getEl().append(me.canvas);
                me._getEl().draggable({
                    containment: me.getBoard()._getWrapper(),
                    addClasses: false,
                    distance: 0,
                    delay: 0,
                    scroll: false,
                    stack: ".chessboard-figure"
                });
            }
            me.canvas.width  = me.getBoard().cellSize;
            me.canvas.height = me.getBoard().cellSize;
            me.canvas.ctx.drawImage(me.getBoard().figuresSprite, me.spriteCoordinates.x, me.spriteCoordinates.y, 120, 120, 0, 0, me.getBoard().cellSize, me.getBoard().cellSize);
            me._getEl().append(me.canvas);
            me._getEl().css({
                position: 'absolute',
                cursor: 'pointer',
                //'background-color' : '#fff999',
                border: 0,
                left: me.getCell().coordinates.offset.left+'px',
                top: me.getCell().coordinates.offset.top+'px',
                width: me.getCell().size.w,
                height: me.getCell().size.h,
                'z-index': 30
            });
        },

        _getEl: function() {
            return this.element;
        },

        _getOpts: function() {
            return this.options;
        },

        _getCanvas: function() {
            return $(this.canvas);
        },

        _updateEl: function(reRender) {
            var me = this;

            if( ! me._getEl()) return;

            if(reRender) me._render();
            me._getEl().css({
                left: me.getCell().coordinates.offset.left+'px',
                top: me.getCell().coordinates.offset.top+'px',
                width: me.getCell().size.w,
                height: me.getCell().size.h,
                'z-index': 30
            });
            me._getCanvas().css({
                width: me.getCell().size.w,
                height: me.getCell().size.h
            });
        },

        _processMove: function(cellFrom, cell, unsafe) {
            var me = this,
            figureX = false,
            tmpfigure = cell.figure;

            if(me.getBoard().moveColor != me.getColor()) return false;
            if(me.getBoard()._getOpts().validateMoves && ! ($.inArray(cell.alias, me.validMoves) > -1)) return false;

            if(unsafe !== false) unsafe = true;

            if(cell.figure) {
                if(cell.figure.getColor() == me.opponent() && cell.figure._isEdible() && me.movesBrokenCells[cell.alias] == cell.alias) {
                    figureX = cell.figure;
                }else{
                    return false;
                }
            }else if(me.movesBrokenCells[cell.alias] && me.movesBrokenCells[cell.alias] != cell.alias) {
                var xcell = me.getBoard().getCell(me.movesBrokenCells[cell.alias]);
                if(xcell.figure && xcell.figure.getColor() == me.opponent() && xcell.figure._isEdible()) {
                    figureX = xcell.figure;
                }
            }
            cellFrom.figure = null;
            me.cell = cell;
            cell.figure = me;
            //me._processMove(cell);

            var mainFigure = me.getBoard().mainFigure[me.getColor()];
            me.getBoard()._updateFiguresMoves();

            if(mainFigure) {
                //mainFigure._onBrokMyCell();
                if( ! mainFigure._isSafeMove(me, cell)) {
                    me._rollbackMove(cellFrom, cell, tmpfigure);
                    //mainFigure._onBrokMyCell();
                    return false;
                }
                /*if( ! mainFigure._isSavingMove(me, cell, me.movesBrokenCells[cell.alias])) {
                    me._rollbackMove(cellFrom, cell, tmpfigure);
                    return false;
                }*/
            }
            if( ! unsafe) {
                me._rollbackMove(cellFrom, cell, tmpfigure);
                return true;
            }

            if(figureX) {

                figureX._x();
                if(xcell) {
                    xcell.figure = null;
                }
                cell.figure = me;
            }


            $.each(me.getBoard()._figures().getAll(), function(i, figure) {
                figure._afterMove();
            });
            return true;
        },

        _rollbackMove: function(cellFrom, cellTo, savedFigure) {
            var me = this;

            cellFrom.figure = me;
            me.cell = cellFrom;
            cellTo.figure = savedFigure;
            me.getBoard()._updateFiguresMoves();
        },

        _x: function() {
            var me = this;

            me.getBoard()._getEl().trigger('figureX', [me]);
            me.destroy();
        },

        _setCell: function(cellFrom, cell) {
            var me = this;

            if((cell && me._processMove(cellFrom, cell))) {
                //me._processMove(cell);
                me.movesCount++;
                me.getBoard()._toggleMoveColor();
                return true;
            }
            return false;
        },

        /*_processMove: function(cell) {
            var me = this;

            me.cell.figure = null;
            me.cell = cell;
            me.cell.figure = me;
            //me.movesCount++;
        },*/

        _brokenCells: function() {
            var me = this,
            processor = {
                clear: function() {
                    me.brokenCells.prev = me.brokenCells.curr;
                    me.brokenCells.curr = []
                },
                add: function(cellAlias) {
                    me.brokenCells.curr.push(cellAlias);
                },
                current: function() {
                    return me.brokenCells.curr;
                },
                previous: function() {
                    return me.brokenCells.prev;
                }
            };
            return processor;
        },

        brokenCells: {
            curr: [],
            prev: []
        },

        _calcMoves: function() {
            var me = this,
                moves = me._calculateMoves();

            this._setCalculatedMoves(moves);
        },

        _valMoves: function() {
            var me = this,
                moves = me._validateMoves();

            this._setValidatedMoves(moves);
        },

        _calculateMoves: function() {
            return [];
        },

        _validateMoves: function() {
            var me = this,
            explorer = me.getBoard()._explorer(me),
            moves = [];

            $.each(me.possibleMoves, function(i, move) {
                moves.push(explorer.lastEmptyOrEdible(move));
            })

            return moves;
        },

        _isEdible: function() {
            return true;
        },

        _setCalculatedMoves: function(moves) {
            var me = this;
            me.possibleMoves = moves;
        },

        _setValidatedMoves: function(moves) {
            var me = this;

            me.validMoves = [];
            me.movesBrokenCells = {};
            $.each(moves, function(i, map) {
                $.each(map, function(y, step) {
                    me.validMoves.push(step.cell.alias);
                    me.movesBrokenCells[step.cell.alias] = step.brokenCellAlias;
                    if(step.brokenCellAlias) {
                        me.getBoard()._brokenCells().add(step.brokenCellAlias, me);
                    }
                });
            });
        },

        _afterValidateMoves: function() {},
        _onMove: function(from, to) {},

        _isSavingMove: function(figure, cell, moveBrokenCell) {
            return true;
        },
        _isSafeMove: function(figure, cell, moveBrokenCell) {
            return true;
        },

        _doMove: function(from, to) {
            var me = this;
            me._getEl().trigger('move', [me.getBoard(), me, from, to]);
            me.getBoard()._getEl().trigger('figuremove', [me.getBoard(), me, from, to]);
            me.getBoard()._updateFiguresMoves();
            //if(to)
            me._onMove(from, to);
        },

        /*_toFront: function() {
            var me = this,
            zoom = 1.02;

            me._getEl().css({
                width: me.getCell().size.w * zoom,
                height: me.getCell().size.h * zoom,
                'z-index': 35
            });
            me._getCanvas().css({
                width: me.getCell().size.w * zoom,
                height: me.getCell().size.h * zoom
            });
        },*/

        /*_onDragStart: function() {
            var me = this;

            me._toFront();
        },*/

        _onDragStop: function(event, ui) {
            var me = this,
            cellFrom = me.getCell(),
            cellTo = me.getBoard().getCellByCoordinates(ui.position.left, ui.position.top);

            if(me._setCell(cellFrom, cellTo)) {
                me._doMove(cellFrom, cellTo);
            };
            me._updateEl(false);
        },

        _onBoardFiguresReady: function() {
            var me = this;
        },

        _afterMove: function() {
            var me = this;

            if(me.getCell().isBroken(me.opponent()) && me.agressors.length > 0) {
                me._onBrokMyCell();
            }
        },

        _onMoveToCell: function(cellAlias, fn) {
            var me = this,
            moveFn = function(event, board, figure, cellFrom, cellTo) {
                if(cellTo.alias == cellAlias) {
                    fn.call();
                    me._getEl().off('move', moveFn);
                }
            }

            me._getEl().on('move', moveFn);
        },

        _onMoveToRow: function(row, fn) {
            var me = this,
            moveFn = function(event, board, figure, cellFrom, cellTo) {
                if(cellTo.row == row) {
                    fn.call();
                    me._getEl().off('move', moveFn);
                }
            }

            me._getEl().on('move', moveFn);
        },

        _onBrokMyCell: function(agressors) {},

        getCell: function() {
            return this.cell;
        },

        getBoard: function() {
            return this.board;
        },

        hasMoves: function() {
            return this.movesCount > 0;
        },

        getType: function() {
            return this._getOpts().type.toUpperCase();
        },

        getColor: function(color) {
            if(color) return color == this._getOpts().color;
            return this._getOpts().color;
        },

        opponent: function() {
            return this.getColor() == 'w' ? 'b' : 'w';
        },

        moveToCell: function(cell) {
            var me = this;

            if(typeof cell == 'string') {
                cell = me.getBoard().getCell(cell);
            }

            var from = me.getCell();

            me._processMove(cell);
            me._getEl().animate({
                left: me.getCell().coordinates.offset.left+'px',
                top: me.getCell().coordinates.offset.top+'px'
            });
            me._doMove(from, cell);
        }


    });
})(jQuery);