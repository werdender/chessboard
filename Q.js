(function($){
    $.widget('chessboard.Q', $.chessboard.piece, {

        _initComponent: function() {
            var me = this;
            me.spriteCoordinates = {x: 120, y: me.spriteLine};
        },

        _calculateMoves: function() {
            var me = this,
            explorer = me.getBoard()._explorer(me),
            nb,
            moves = [
                explorer.diagonal('front', 'left'),
                explorer.diagonal('front', 'right'),
                explorer.diagonal('back', 'left'),
                explorer.diagonal('back', 'right'),
                explorer.vertical('front'),
                explorer.vertical('back'),
                explorer.horizontal('left'),
                explorer.horizontal('right')
            ];

            return moves;
        }

    });
})(jQuery);