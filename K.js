(function($){
    $.widget('chessboard.K', $.chessboard.piece, {

        _initComponent: function() {
            var me = this;

            me.spriteCoordinates = {
                x: 0,
                y: me.spriteLine
            };
            me.kingReshuffleCell = false;
            me.kingRookReshuffleCell = false;
            me.queenReshuffleCell = false;
            me.queenRookReshuffleCell = false;
            me.kingRook = false;
            me.queenRook = false;
            me.alias = me.getColor() + '-King';
            me.getBoard().mainFigure[me.getColor()] = me;
            me.agressors = [];
            me.getBoard()._getEl().on('check', $.proxy(me._onCheck, me));
        },

        _calculateMoves: function() {
            var me = this,
            explorer = me.getBoard()._explorer(me),
            moves = [
                explorer.diagonal('front', 'left', 1),
                explorer.diagonal('front', 'right', 1),
                explorer.diagonal('back', 'left', 1),
                explorer.diagonal('back', 'right', 1),
                explorer.vertical('front', 1),
                explorer.vertical('back', 1),
                explorer.horizontal('left', 1),
                explorer.horizontal('right', 1)
            ];

            return moves;
        },

        _afterValidateMoves: function() {
            var me = this,
            explorer = me.getBoard()._explorer(me);
            if(me.getColor('w')) {
                var kingDirection = 'right',
                queenDirection = 'left',
                queenReshuffleCellAliasForKing = 'c1',
                queenReshuffleCellAliasForRook = 'd1',
                kingReshuffleCellAliasForKing = 'g1',
                kingReshuffleCellAliasForRook = 'f1';
            }else{
                var kingDirection = 'left',
                queenDirection = 'right',
                queenReshuffleCellAliasForKing = 'c8',
                queenReshuffleCellAliasForRook = 'd8',
                kingReshuffleCellAliasForKing = 'g8',
                kingReshuffleCellAliasForRook = 'f8';
            }

            if(me.movesCount === 0 && ! me.getCell().isBroken(me.opponent())) {// reshuffle
                var kingRookLine = explorer.horizontal(kingDirection),
                queenRookLine = explorer.horizontal(queenDirection),
                kingRook,
                queenRook;

                if( ! me.getBoard().getCell(kingReshuffleCellAliasForKing).isBroken(me.opponent())) {
                    $.each(kingRookLine, function(i, step) {
                        if((step.cell.figure && step.cell.figure.getType() != 'R') || step.cell.isBroken(me.opponent())) {
                            me.canKingReshuffle = false;
                            return false;
                        }else if(step.cell.figure && step.cell.figure.getType() == 'R' && step.cell.figure.getColor(me.getColor()) && step.cell.figure._isCanReshuffle()) {
                            me.canKingReshuffle = true;
                            kingRook = step.cell.figure;
                        }
                    });
                }
                if( ! me.getBoard().getCell(queenReshuffleCellAliasForKing).isBroken(me.opponent())) {
                    $.each(queenRookLine, function(i, step) {
                        if((step.cell.figure && step.cell.figure.getType() != 'R') || step.cell.isBroken(me.opponent())) {
                            me.canQueenReshuffle = false;
                            return false;
                        }else if(step.cell.figure && step.cell.figure.getType() == 'R' && step.cell.figure.getColor(me.getColor()) && step.cell.figure._isCanReshuffle()) {
                            me.canQueenReshuffle = true;
                            queenRook = step.cell.figure;
                        }
                    });
                }
                if(me.canKingReshuffle && ! me.getBoard().getCell(kingReshuffleCellAliasForKing).isBroken(me.opponent())) {
                    me.kingReshuffleCell = kingReshuffleCellAliasForKing;
                    me.kingRookReshuffleCell = kingReshuffleCellAliasForRook;
                    me.kingRook = kingRook;
                    me.validMoves.push(me.kingReshuffleCell);
                    me.movesBrokenCells[me.kingReshuffleCell] = false;
                }else{
                    me.canKingReshuffle = false;
                }
                if(me.canQueenReshuffle && ! me.getBoard().getCell(queenReshuffleCellAliasForKing).isBroken(me.opponent())) {
                    me.queenReshuffleCell = queenReshuffleCellAliasForKing;
                    me.queenRookReshuffleCell = queenReshuffleCellAliasForRook;
                    me.queenRook = queenRook;
                    me.validMoves.push(me.queenReshuffleCell);
                    me.movesBrokenCells[me.queenReshuffleCell] = false;
                }else{
                    me.canQueenReshuffle = false;
                }
            }

        },

        _isEdible: function() {
            return false;
        },

        _onMove: function(cellFrom, cellTo) {
            var me = this;

            if(cellTo.alias == me.kingReshuffleCell && me.canKingReshuffle && me.kingRook) {
                me.kingRook.moveToCell(me.kingRookReshuffleCell);
                me.getBoard()._getEl().trigger('reshuffle', ['O-O', me, me.kingRook, me.getColor()]);
            }

            if(cellTo.alias == me.queenReshuffleCell && me.canQueenReshuffle && me.queenRook) {
                me.queenRook.moveToCell(me.queenRookReshuffleCell);
                me.getBoard()._getEl().trigger('reshuffle', ['O-O-O', me, me.queenRook, me.getColor()]);
            }
        },

        _onBrokMyCell: function() {
            var me = this;

            me.getBoard()._getEl().trigger('check', [me]);
        },

        _onCheck: function(event, king) {
            var me = this,
            checkmate = true;
            if(king.componentId == me.componentId) {
                setTimeout(function() {
                    $.each(me.getBoard()._figures().get(me.getColor()), function(i, figure) {
                        var cellFrom = figure.getCell();
                        $.each(figure.validMoves, function(y, move) {
                            var cellTo = me.getBoard().getCell(move);
                            if(figure._processMove(cellFrom, cellTo, false)) {
                                checkmate = false;
                                return false;
                            }
                        });
                        if( ! checkmate) return false;
                    });
                    if(checkmate) {
                        me.getBoard()._getEl().trigger('gameover', [me]);
                    }
                }, 100);
            }
        },

        _isSavingMove: function(figure, cell, forCheckmate) {

            var me = this,
            agressors = me.agressors,
            safe = false;

            if(agressors.length < 1) return true;
            if(agressors.length > 1 && figure.componentId != me.componentId) return false;

            $.each(agressors, function(i, agressor) {
                if(agressor.getCell().alias == cell.alias) {
                    safe = true;
                }
            });
            if(safe && ! forCheckmate) me.agressors = [];
            return safe;
        },

        _isSafeMove: function(figure, cell, forCheckmate) {
            var me = this;

            if( ! forCheckmate) forCheckmate = false;
            if(me.getCell().isBroken(me.opponent()) && ! me._isSavingMove(figure, cell, forCheckmate)) return false;
            return true;
        }

    });
})(jQuery);