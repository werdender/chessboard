(function($){
    $.widget('chessboard.P', $.chessboard.piece, {

        _initComponent: function() {
            var me = this;
            me.spriteCoordinates = {x: 600, y: me.spriteLine};

            var reincarnationRow = me.getColor('w') ? 8 : 1,
            passedPawnRow = me.getColor('w') ? 4 : 5;

            me._onMoveToRow(reincarnationRow, function() {
                me.getBoard()._getEl().trigger('reincarnation', me);
            });

            me._onMoveToRow(passedPawnRow, function() {
                if(me.movesCount === 1) {

                    me.isPassedPawn = true;
                    me.passedMove = me.getColor('w') ? me.getBoard().blackMovesCount + 1 : me.getBoard().whiteMovesCount + 1;
                }
            });
        },

        _calculateMoves: function() {
            var me = this,
            explorer = me.getBoard()._explorer(me),
            nb,
            moves = [
                explorer.custom({
                    vertical: {direction: 'front', count: 1},
                    horizontal: {direction: 'left', count: 0}
                }, false),
                explorer.custom({
                    vertical: {direction: 'front', count: 1},
                    horizontal: {direction: 'left', count: 1}
                }),
                explorer.custom({
                    vertical: {direction: 'front', count: 1},
                    horizontal: {direction: 'right', count: 1}
                })
            ];

            var leftCell = explorer.custom({
                vertical: {direction: 'front', count: 0},
                horizontal: {direction: 'left', count: 1}
            })[0],
            rightCell = explorer.custom({
                vertical: {direction: 'front', count: 0},
                horizontal: {direction: 'right', count: 1}
            })[0];

            var myColorMovesCount = me.getColor('w') ? me.getBoard().whiteMovesCount : me.getBoard().blackMovesCount;

            if(leftCell && leftCell.cell.figure) {
                if(leftCell.cell.figure.isPassedPawn) {
                    if(myColorMovesCount + 1 == leftCell.cell.figure.passedMove) {

                        moves.push(explorer.custom({
                            vertical: {direction: 'front', count: 1},
                            horizontal: {direction: 'left', count: 1}
                        }, {
                            vertical: {direction: 'front', count: 0},
                            horizontal: {direction: 'left', count: 1}
                        }));
                    }
                }
            }

            if(rightCell && rightCell.cell.figure) {
                if(rightCell.cell.figure.isPassedPawn) {
                    if(myColorMovesCount + 1 == rightCell.cell.figure.passedMove) {
                        moves.push(explorer.custom({
                            vertical: {direction: 'front', count: 1},
                            horizontal: {direction: 'right', count: 1}
                        }, {
                            vertical: {direction: 'front', count: 0},
                            horizontal: {direction: 'right', count: 1}
                        }));
                    }
                }
            }

            var line = me.getColor('w') ? 2 : 7;

            if(me.movesCount === 0 && me.getCell().row == line) { // double move if is a first move
                moves.push(explorer.custom({
                    vertical: {direction: 'front', count: 2},
                    horizontal: {direction: 'left', count: 0}
                }, false));
            }

            return moves;
        },

        _validateMoves: function() {
            var me = this,
            moves = [];

            $.each(me.possibleMoves, function(i, move) {
                $.each(move, function(y, step) {
                    if(step.cell.figure) {
                        if(step.cell.figure.getColor() == me.opponent()
                        && step.cell.alias == step.brokenCellAlias) {

                            moves.push(move);
                        }
                    }else{
                        if( ! step.brokenCellAlias || step.cell.alias != step.brokenCellAlias) {
                            moves.push(move);
                        }
                    }
                });
            });

            return moves;
        }



    });
})(jQuery);