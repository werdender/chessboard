(function($){
    $.widget('chessboard.N', $.chessboard.piece, {

        _initComponent: function() {
            var me = this;
            me.spriteCoordinates = {x: 360, y: me.spriteLine};
        },

        _calculateMoves: function() {
            var me = this,
            explorer = me.getBoard()._explorer(me),
            nb,
            moves = [
                explorer.custom({
                    vertical: {direction: 'front', count: 2},
                    horizontal: {direction: 'left', count: 1}
                }),
                explorer.custom({
                    vertical: {direction: 'front', count: 1},
                    horizontal: {direction: 'left', count: 2}
                }),
                explorer.custom({
                    vertical: {direction: 'front', count: 2},
                    horizontal: {direction: 'right', count: 1}
                }),
                explorer.custom({
                    vertical: {direction: 'front', count: 1},
                    horizontal: {direction: 'right', count: 2}
                }),
                explorer.custom({
                    vertical: {direction: 'back', count: 2},
                    horizontal: {direction: 'left', count: 1}
                }),
                explorer.custom({
                    vertical: {direction: 'back', count: 1},
                    horizontal: {direction: 'left', count: 2}
                }),
                explorer.custom({
                    vertical: {direction: 'back', count: 2},
                    horizontal: {direction: 'right', count: 1}
                }),
                explorer.custom({
                    vertical: {direction: 'back', count: 1},
                    horizontal: {direction: 'right', count: 2}
                })
            ];

            return moves;
        }

    });
})(jQuery);